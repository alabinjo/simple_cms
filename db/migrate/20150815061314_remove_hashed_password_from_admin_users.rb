class RemoveHashedPasswordFromAdminUsers < ActiveRecord::Migration
  def change
    remove_column :admin_users, :hashed_password, :string, limit: 40
  end
end
