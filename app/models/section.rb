class Section < ActiveRecord::Base
 
  
  belongs_to :page
  has_many :section_edits
  has_many :editors, :through => :section_edits, :class_name => "AdminUser"
  
  CONTENT_TYPES = ["Text", "HTML"]
  validates :name, presence: true, length: { maximum: 255 }
  validates :content, presence: true 
  validaes :contenT_type, inclusion: { in: CONTENT_TYPES }, 
                            message: "must  be one of #{CONTENT_TYPES.join(", ")} "
  
  scope :visible, lambda { where(:visible => true) }
  scope :invisible, lambda { where(:visible => false) }
  scope :sorted, lambda { order("sections.position ASC") }
  scope :newest_first, lambda { order("sections.created_at DESC")}
end
