class AdminUser < ActiveRecord::Base
  #to configure a different table name 
  #self.table_name :admin_users
  
  scope :sorted, lambda { order ("admin_user.last_name ASC, admin_user.first_name ASC") }

  has_and_belongs_to_many :pages
  has_many :section_edits
  has_many :sections, :through => :section_edits
  
  has_secure_password
  EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
  validates :first_name, presence: true,  length: { maximum: 25 }
  validates :last_name,  presence: true,  length: { maximum: 50 }
  validates :username,   presence: true,  length: { within: 3..10 }, uniqueness: true 
  validates :email,      presence: true,  length: { maximum: 100 }, format: { with: EMAIL_REGEX }, 
                                                                    confirmation: true 
                                                                    
  def self.full_name
    "#{admin_user.first_name} #{admin_user.last_name}"
  end 
  
  
  
end
