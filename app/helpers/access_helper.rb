module AccessHelper
  
  def log_in(admin_user)
    session[:admin_user_id] = admin_user.id
  end 
  
  def log_out(user)
    session[:user_id] = nil
  end 
end
