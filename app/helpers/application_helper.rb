module ApplicationHelper
   include AccessHelper 
   
   def error_messages_for(object)
      render partial: "application/errors", locals: { object: object }
   end 
   
   def status_tag(boolean, options={})
    	options[:true_text]  ||= ''
    	options[:false_test] ||= ''

  	if boolean
  		content_tag(:span, options[:true_text], :class => 'status true')
  	else 
  		content_tag(:span, options[:true_text], :class => 'status false')
  	end 
  end  
end
