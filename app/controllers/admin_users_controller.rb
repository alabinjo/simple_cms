class AdminUsersController < ApplicationController
  layout "admin"
  
  before_action :confirm_logged_in
  
  def index
    @admin_users = AdminUser.sorted 
  end 
  
  def new
    @admin_user = AdminUser.new 
  end 
  
  def create
    @admin_user = AdminUser.new(admin_user_params)
    if @admin_user.save
      flash[:notice] = "A new user has been created"
      redirect_to subjects_path
    else 
      flash.now[:notice] = "An error creating a user"
      render :new 
    end 
  end 
  
  def edit
    @admin_user = AdminUser.find(admin_user_params)
  end 
  
  def update 
    @admin_user = AdminUser.find(params[:id])
    if @admin_user.update(admin_user_params)
      flash[:notice] = "User has been updated succesfully"
      redirect_to index_path
    else 
      flash.now[:notice] = "Something went wrong. Try again"
      render :edit 
    end 
  end 
  
  #this just fetches the user to be deleted thats it 
  def delete
    @admin_user = AdminUser.find(params[:id])
  end
  
  def destroy 
    @admin_user = AdminUser.find(admin_user_params)
    @admin_user.destroy 
  end 
  
  private 
  
  def admin_user_params
    require(:admin_user).permit(:first_name, :last_name, :username, :email)
  end 
  
end
