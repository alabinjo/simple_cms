class SectionsController < ApplicationController
  
  layout 'admin'
  
  before_action :confirm_logged_in
  
  def index 
    @sections = Section.all
  end 
  
  def show
    @section = Section.find(params[:id])
  end 
  
  def new 
    @section = Section.new 
  end 
  
  def create
    @section = Section.new(section_params)
    if @section.save
      flash[:notice] = "Section Created Successfully"
      redirect_to sections_path
    else 
      flash.now[:error] = "Section error"
      render :new 
    end 
  end 
  
  def edit 
    @section = Section.find(params[:id])
  end 
  
  def update 
    @section = Section.find(params[:id])
    if @section.update(section_params)
      flash[:notice] = "Section Updated Successfully"
      redirect_to sections_path(@section)
    else
      flash.now[:error] = "Error. Please fix."
      render :edit 
    end 
  end 
  
  def delete
    @section = Section.find(params[:id])
  end 
  
  def destroy
    @section = Section.find(params[:id])
    @section.destroy 
    flash[:notice] = "Section #{@section.name} has been deleted successfully"
    redirect_to sections_path 
  end 
  
  private 
  
  def section_params
    params.require(:section).permit(:page_id, :name, :position, :visible, :content_type, :content, :created_at, :updated_at)
  end 
  
end
