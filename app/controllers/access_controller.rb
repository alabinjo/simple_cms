class AccessController < ApplicationController
  
  before_action :confirm_logged_in, except: [:new, :create, :logout]
  def index
    #display text and links 
  end

  #def new
  #end 
  
  #def login
  def new
    #display and login form 
  end
  
 #def attempt_login
  def create 
    #pulls the users out of the database by emailt
    admin_user = AdminUser.find_by(params[:username])
    # admin_user - checks to see if there is user with that email 
    # if email is valid it will return true and proceed to authentication 
    # if admin_user returns false the user cannot be authenticated 
    # admin_user.authenticate - checks to see if the email matches the one 
    # in the database 
    if admin_user && admin_user.authenticate(params[:password])
      flash[:notice] = "Login Successful"
      #places a temporary cookie on the users browser
      #contains the encrypted version of the users id
      session[:admin_user_id] = admin_user.id
      session[:username] = admin_user.username
      redirect_to admin_path
    else 
      flash.now["notice"] = "Invalid password/email combination"
      render :broken
    end 
  end 

  def logout
    session[:admin_user_id] = nil
    session[:username] = nil
    flash[:notice] = "Succesfully Logged out"
    redirect_to login_path 
  end 
end

  
  end 
