class PagesController < ApplicationController
  
   layout 'admin'
   
   before_action :confirm_logged_in
   
  
  def index 
    @pages = Page.all
  end 
  
  def show
    @page = Page.find(params[:id])
  end 
  
  def new 
    @page = Page.new 
    @subjects = Subject.all
    @page_count = Page.count + 1
  end 
  
  def create
    @page = Page.new(page_params)
    if @page.save
      flash[:notice] = "Page Created Successfully"
      redirect_to pages_path
    else 
      flash.now[:error] = "Page error"
      @subjects = Subject.all
      @page_count = Page.count + 1
      render :new 
    end 
  end 
  
  def edit 
    @page = Page.find(params[:id])
    @subjects = Subject.all
    @page_count = Page.count
  end 
  
  def update 
    @page = Page.find(params[:id])
    if @page.update(page_params)
      flash[:notice] = "Page Updated Successfully"
      redirect_to pages_path(@page)
    else
      flash.now[:error] = "Error. Please fix."
      @subjects = Subject.all
      @page_count = Page.count
      render :edit 
    end 
  end 
  
  def delete
    @page = Page.find(params[:id])
  end 
  
  def destroy
    @page = Page.find(params[:id])
    @page.destroy 
    flash[:notice] = "Page #{@page.name} has been deleted successfully"
    redirect_to pages_path 
  end 
  
  private 
  
  def page_params
    params.require(:page).permit(:subject_id, :name, :permalink, :position, :visible)
  end 
  
  
  
  

end
