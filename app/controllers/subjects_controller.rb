class SubjectsController < ApplicationController
  
  layout 'admin'
  
  before_action :confirm_logged_in
  
  
  def index 
    @subjects = Subject.all 
  end 
  
  def show 
    @subject = Subject.find(params[:id])
  end 
  
  def new 
    @subject = Subject.new 
    @subject_count = Subject.count + 1
  end 
  
  def create 
    @subject = Subject.new(subjects_param)
    if @subject.save 
      flash[:notice] = "Subject Created Succesfully"
      redirect_to subjects_path
    else 
      flash.now[:error] = "Error. Please try again"
      @subject_count = Subject.count + 1
      render "new"
    end 
  end 
  
  def edit 
    @subject = Subject.find(params[:id])
    @subject_count = Subject.count 
  end 
  
  def update 
    @subject = Subject.find(params[:id])
    if @subject.update(subjects_param)
      flash[:notice] = "Subject Updated Succesfully"
      redirect_to subject_path(@subject)
    else
      @subject_count = Subject.count 
      render :edit 
    end 
  end 
  
  def destroy
    @subject = Subject.find(params[:id])
    @subject.destroy 
    flash[:notice] = "Subject #{@subject.name} Destroyed Succesfully"
    redirect_to subjects_path 
  end 
  
  private 
  
  def subjects_param 
    params.require(:subject).permit(:name, :position, :visible, :created_at)
  end 
end
